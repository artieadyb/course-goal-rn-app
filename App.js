import { useState } from 'react';
import { StyleSheet, Text, TextInput, View, Button, ScrollView, FlatList } from 'react-native';

export default function App() {
  const [goalsValue, setGoalsValue] = useState('')
  const [courseGoal, setCourseGoal] = useState([])

  //try to use arrow function
  const goalsInputHandler = (value)=>{
    setGoalsValue(value)
  }

  //try to use function
  function addGoalsHandler(){
    //setCourseGoal((currentCourseGoal)=>[...currentCourseGoal, goalsValue])
    setCourseGoal((currentCourseGoal)=>[...currentCourseGoal, {text: goalsValue, id: Math.random().toString()}])
  }

  return (
    <View style={styles.appContainer}>
      <View style={styles.inputContainer}>
        <TextInput style={styles.textInput} placeholder='Your Course Goal!' onChangeText={goalsInputHandler}/>
        <Button title='Add Goal' onPress={addGoalsHandler}/>
      </View>
      <View style={styles.goalsContainer}>
        {/* <ScrollView alwaysBounceVertical={false}>
          {courseGoal.map((goal, index)=>
            <View style={styles.goalItem} key={index}>
              <Text style={styles.goalText}>{goal}</Text>
            </View>
          )}
        </ScrollView> */}
        <FlatList
          data={courseGoal}
          renderItem={(itemData)=>{
            return(
              <View style={styles.goalItem}>
                <Text style={styles.goalText}>{itemData.item.text}</Text>
              </View>
            )
          }
          }
          keyExtractor={(item, index)=>{
            return item.id
          }}
          alwaysBounceVertical={false}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  appContainer:{
    flex:1,
    paddingTop:50,
    paddingHorizontal:16,
  },
  inputContainer:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom:24,
    borderBottomColor:'#cccccc',
    borderBottomWidth:1,
  },
  textInput:{
    borderWidth:1,
    borderColor: '#cccccc',
    width:'70%',
    marginRight:8,
    padding:8,
  },
  goalsContainer:{
    flex:5,
  },
  goalItem:{
    margin:8,
    padding:8,
    borderRadius:6,
    backgroundColor:'#5e0acc',
  },
  goalText:{
    color:'white',
  }
});
